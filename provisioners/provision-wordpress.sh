##Update repos
sudo apt-get update

##Install wordpress prereqs: MySQL and PHP
echo "installing packages..."
echo "mysql-server mysql-server/root_password password testpass" | sudo debconf-set-selections
echo "mysql-server mysql-server/root_password_again password testpass" | sudo debconf-set-selections
sudo apt-get install -y mysql-server php5 php5-mysql

##Start our services and make them load on boot
echo "turning on mysql/apache..."
sudo service mysql start
sudo service apache2 start

##Create wp database and a wpuser to talk to db
echo "setting up mysql db..."
mysql -uroot -ptestpass -e "CREATE database wordpress"
mysql -uroot -ptestpass -e "CREATE USER 'wpuser'@'localhost' IDENTIFIED BY 'testpass';"
mysql -uroot -ptestpass -e "GRANT ALL PRIVILEGES ON wordpress.* TO wpuser@localhost;"
mysql -uroot -ptestpass -e "FLUSH PRIVILEGES;"

##Lay down Wordpress src files
echo "lay down wordpress packages... "
wget https://wordpress.org/latest.tar.gz
sudo tar -xf latest.tar.gz -C /var/www/html
ls -lah /var/www/html
rm latest.tar.gz

##Create the wp-config file and edit with proper user vals
echo "configure wp-config.php..."
sudo cp /var/www/html/wordpress/wp-config-sample.php /var/www/html/wordpress/wp-config.php
sudo chown nobody:nogroup /var/www/html/wordpress/wp-config.php
cd /var/www/html/wordpress/
sudo sed -i 's/database_name_here/wordpress/' wp-config.php
sudo sed -i 's/username_here/wpuser/' wp-config.php
sudo sed -i 's/password_here/testpass/' wp-config.php

##Shut down services
sudo service mysql stop
sudo service apache2 stop
